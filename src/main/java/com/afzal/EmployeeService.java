package com.afzal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EmployeeService {
	static HashMap<Integer,Employee> empMap;
	public EmployeeService() {
//		System.out.println("Constructor service calleds");
		empMap = new HashMap<>();
		Employee e1 = new Employee(
				1,
				"Afzal",
				"afzal@gmail.com",
				500000
				);
		Employee e2 = new Employee(
				2,
				"Afaan",
				"afaan@gmail.com",
				400000
				);
		Employee e3 = new Employee(
				3,
				"Eshan",
				"eshan@gmail.com",
				300000
				);
		Employee e4 = new Employee(
				4,
				"Asif",
				"asif@gmail.com",
				600000
				);
		empMap.put(e1.getEmployee_id(), e1);
		empMap.put(e2.getEmployee_id(), e2);
		empMap.put(e3.getEmployee_id(), e3);
		empMap.put(e4.getEmployee_id(), e4);
	}
// get all employee
	public List<Employee> getAllEmployees(){
		  List<Employee> employees = new ArrayList<>(empMap.values());
		  return employees;
	}
//	 add a employee
	public Employee addEmployee(Employee emp) {
		if (empMap.get(emp.getEmployee_id()) != null) return empMap.get(emp.getEmployee_id());
		else {
		empMap.put(emp.getEmployee_id(),emp);
		return empMap.get(emp.getEmployee_id());
		}
	}
// get an employee
	public Employee getEmployee(int id) {
		if (empMap.get(id) != null) 
		return empMap.get(id);
		else 
			return new Employee();
		
	}
//	update employee name
	public Employee updateEmployeeName(int id, String name) {
		Employee emp = empMap.get(id);
		emp.setEmployee_name(name);
		empMap.put(id, emp);
		return emp;
	}
//	update employee email
	public Employee updateEmployeeEmail(int id, String email) {
		Employee emp = empMap.get(id);
		emp.setEmployee_emailid(email);
		empMap.put(id, emp);
		return emp;
	}
//	update employee salary
	public Employee updateEmployeeSalary(int id, double salary) {
		Employee emp = empMap.get(id);
		emp.setEmployee_salary(salary);
		empMap.put(id, emp);
		return emp;
	}
//	update a employee record fully
	public String updateEmployee(Employee emp) {
		empMap.put(emp.getEmployee_id(),emp);
		return "Employee Updated Successfully";
	}
//	delete employee
	public Employee deleteEmployee(int id) {
		Employee emp = empMap.get(id);
		empMap.remove(id);
		return emp;
	}
}

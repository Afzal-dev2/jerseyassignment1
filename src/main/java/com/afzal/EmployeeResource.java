package com.afzal;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/employees")
public class EmployeeResource {
	EmployeeService empServ;
	public EmployeeResource() {
//		System.out.println("Inside resource constructor");
		empServ = new EmployeeService();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/all")
	public List<Employee> getAllEmployees(){
		return empServ.getAllEmployees();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/emp/{id}")
	public Employee getEmployee(@PathParam("id") int id_) {
		return empServ.getEmployee(id_);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/addemployee")
	public Employee addEmployee(Employee emp) {
		return empServ.addEmployee(emp);
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteemployee/{id}")
	public Employee deleteEmployee(@PathParam("id") int id_) {
		return empServ.deleteEmployee(id_);
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateemployeename/{id}/{name}")
	public Employee updateEmployeeName(@PathParam("id") int id_, @PathParam("name") String name_) {
		return empServ.updateEmployeeName(id_,name_);
	}
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateemployeemail/{id}/{email}")
	public Employee updateEmployeeEmail(@PathParam("id") int id_, @PathParam("email") String email_) {
		return empServ.updateEmployeeEmail(id_,email_);
	}
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateemployeesalary/{id}/{salary}")
	public Employee updateEmployeeSalary(@PathParam("id") int id_, @PathParam("salary") double salary_) {
		return empServ.updateEmployeeSalary(id_,salary_);
	}
	
	
}

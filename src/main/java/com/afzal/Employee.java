package com.afzal;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Employee {
	public Employee(int id, String name, String email, double salary){
		
		this.employee_emailid = email;
		this.employee_id = id;
		this.employee_name = name;
		this.employee_salary = salary;
	}
	 public Employee() {
		// TODO Auto-generated constructor stub
	}
	public int getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}
	public String getEmployee_name() {
		return employee_name;
	}
	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}
	public String getEmployee_emailid() {
		return employee_emailid;
	}
	public void setEmployee_emailid(String employee_emailid) {
		this.employee_emailid = employee_emailid;
	}
	public double getEmployee_salary() {
		return employee_salary;
	}
	public void setEmployee_salary(double employee_salary) {
		this.employee_salary = employee_salary;
	}
	private int employee_id;
     @Override
	public String toString() {
		return "Employee [employee_id=" + employee_id + ", employee_name=" + employee_name + ", employee_emailid="
				+ employee_emailid + ", employee_salary=" + employee_salary + "]";
	}
	private String employee_name;
     private String employee_emailid;
     private double employee_salary;

}
